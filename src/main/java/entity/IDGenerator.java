package entity;

import java.util.UUID;

public class IDGenerator {
	public static String UUID() {
		return UUID.randomUUID().toString();
	}
}
