package entity;

public class FileStatus {
	public static int PENDING = 0x01;
	public static int ACCEPTED = 0x02;
	public static int REVOKED = 0x04;
}
