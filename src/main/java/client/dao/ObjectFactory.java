
package client.dao;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the client.dao package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: client.dao
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UserFile }
     * 
     */
    public UserFile createUserFile() {
        return new UserFile();
    }

    /**
     * Create an instance of {@link UserFileArray }
     * 
     */
    public UserFileArray createUserFileArray() {
        return new UserFileArray();
    }

    /**
     * Create an instance of {@link File }
     * 
     */
    public File createFile() {
        return new File();
    }

    /**
     * Create an instance of {@link SharedFileArray }
     * 
     */
    public SharedFileArray createSharedFileArray() {
        return new SharedFileArray();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link SharedFile }
     * 
     */
    public SharedFile createSharedFile() {
        return new SharedFile();
    }

    /**
     * Create an instance of {@link FileContent }
     * 
     */
    public FileContent createFileContent() {
        return new FileContent();
    }

}
