
package client.dao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>userFile complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="userFile">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="directory" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="fileId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="parentId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="permission" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userFile", propOrder = {
    "directory",
    "fileId",
    "fileName",
    "id",
    "owner",
    "parentId",
    "permission",
    "status",
    "userId"
})
public class UserFile {

    protected boolean directory;
    protected String fileId;
    protected String fileName;
    protected long id;
    protected boolean owner;
    protected long parentId;
    protected int permission;
    protected int status;
    protected String userId;

    /**
     * 获取directory属性的值。
     * 
     */
    public boolean isDirectory() {
        return directory;
    }

    /**
     * 设置directory属性的值。
     * 
     */
    public void setDirectory(boolean value) {
        this.directory = value;
    }

    /**
     * 获取fileId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * 设置fileId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileId(String value) {
        this.fileId = value;
    }

    /**
     * 获取fileName属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * 设置fileName属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * 获取id属性的值。
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * 设置id属性的值。
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * 获取owner属性的值。
     * 
     */
    public boolean isOwner() {
        return owner;
    }

    /**
     * 设置owner属性的值。
     * 
     */
    public void setOwner(boolean value) {
        this.owner = value;
    }

    /**
     * 获取parentId属性的值。
     * 
     */
    public long getParentId() {
        return parentId;
    }

    /**
     * 设置parentId属性的值。
     * 
     */
    public void setParentId(long value) {
        this.parentId = value;
    }

    /**
     * 获取permission属性的值。
     * 
     */
    public int getPermission() {
        return permission;
    }

    /**
     * 设置permission属性的值。
     * 
     */
    public void setPermission(int value) {
        this.permission = value;
    }

    /**
     * 获取status属性的值。
     * 
     */
    public int getStatus() {
        return status;
    }

    /**
     * 设置status属性的值。
     * 
     */
    public void setStatus(int value) {
        this.status = value;
    }

    /**
     * 获取userId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * 设置userId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

}
