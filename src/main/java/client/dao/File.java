
package client.dao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>file complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="file">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="createTime" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="fileId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fileSize" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ownerUserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="permission" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "file", propOrder = {
    "createTime",
    "fileId",
    "fileSize",
    "ownerUserId",
    "permission"
})
public class File {

    protected long createTime;
    protected String fileId;
    protected long fileSize;
    protected String ownerUserId;
    protected int permission;

    /**
     * 获取createTime属性的值。
     * 
     */
    public long getCreateTime() {
        return createTime;
    }

    /**
     * 设置createTime属性的值。
     * 
     */
    public void setCreateTime(long value) {
        this.createTime = value;
    }

    /**
     * 获取fileId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * 设置fileId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileId(String value) {
        this.fileId = value;
    }

    /**
     * 获取fileSize属性的值。
     * 
     */
    public long getFileSize() {
        return fileSize;
    }

    /**
     * 设置fileSize属性的值。
     * 
     */
    public void setFileSize(long value) {
        this.fileSize = value;
    }

    /**
     * 获取ownerUserId属性的值。
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerUserId() {
        return ownerUserId;
    }

    /**
     * 设置ownerUserId属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerUserId(String value) {
        this.ownerUserId = value;
    }

    /**
     * 获取permission属性的值。
     * 
     */
    public int getPermission() {
        return permission;
    }

    /**
     * 设置permission属性的值。
     * 
     */
    public void setPermission(int value) {
        this.permission = value;
    }

}
