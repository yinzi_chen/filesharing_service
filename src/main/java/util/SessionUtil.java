package util;

import java.io.Serializable;

import org.hibernate.Session;

public class SessionUtil {
	public static Object create(String key, Object obj) {
		Session session = HibernateUtil.getSessionFactory(key).openSession();
		try {
			session.beginTransaction();

			session.save(obj);

			session.getTransaction().commit();
		} catch (Throwable e) {
			// e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return obj;
	}

	public static Object update(String key, Object obj) {
		Session session = HibernateUtil.getSessionFactory(key).openSession();
		try {
			session.beginTransaction();

			session.update(obj);

			session.getTransaction().commit();
		} catch (Throwable e) {
			// e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return obj;
	}

	public static Object get(String key, Class<? extends Object> c,
			Serializable objKey) {
		Session session = HibernateUtil.getSessionFactory(key).openSession();
		try {
			session.beginTransaction();

			Object result = session.get(c, objKey);

			session.getTransaction().commit();
			return result;
		} catch (Throwable e) {
			// e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public static boolean delete(String key, Object obj) {
		Session session = HibernateUtil.getSessionFactory(key).openSession();
		try {
			session.beginTransaction();

			session.delete(obj);

			session.getTransaction().commit();
		} catch (Throwable e) {
			// e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
		return true;
	}
}
