package util;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import service.ServiceConfig;

public class HibernateUtil {

	private static List<String> dbUrls;
	private static final List<SessionFactory> sessionFactories = buildSessionFactories();

	private static List<SessionFactory> buildSessionFactories() {
		List<SessionFactory> sessionFactories = new ArrayList<SessionFactory>();
		dbUrls = new ArrayList<String>();
		try {
			String[] dbs = ServiceConfig.get("db").split(";");
			for (String db : dbs) {
				Configuration configuration = new Configuration().configure();
				configuration.setProperty("hibernate.connection.url", db);
				sessionFactories.add(configuration.buildSessionFactory());
				dbUrls.add(db);
			}
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err
					.println("Initial SessionFactories creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
		return sessionFactories;
	}

	public static void closeSessionFactories() {
		for (SessionFactory factory : sessionFactories) {
			factory.close();
		}
	}

	public static SessionFactory getSessionFactory(String key) {
		int idx = Math.abs(key.hashCode()) % sessionFactories.size();
		System.out.println("Sending request to " + dbUrls.get(idx));
		return sessionFactories.get(idx);
	}

}
