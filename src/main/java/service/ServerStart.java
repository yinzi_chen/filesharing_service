package service;

import javax.xml.ws.Endpoint;

import service.ao.FileSharingAO;
import service.ao.impl.FileSharingAOImpl;
import service.dao.FileSharingDAO;
import service.dao.impl.FileSharingDAOImpl;

public class ServerStart {
	public static void main(String[] args) throws Exception {
		String[] urls;

		FileSharingDAO dao = new FileSharingDAOImpl();
		urls = ServiceConfig.get("FileSharingDAO").split(";");
		for (String url : urls) {
			Endpoint.publish(url, dao);
			System.out.println("service published at: " + url);
		}
		FileSharingAO ao = new FileSharingAOImpl();
		urls = ServiceConfig.get("FileSharingAO").split(";");
		for (String url : urls) {
			Endpoint.publish(url, ao);
			System.out.println("service published at: " + url);
		}
	}

}
