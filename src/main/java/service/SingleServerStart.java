package service;

import javax.xml.ws.Endpoint;

import service.ao.FileSharingAO;
import service.ao.impl.FileSharingAOImpl;
import service.dao.FileSharingDAO;
import service.dao.impl.FileSharingDAOImpl;

public class SingleServerStart {

	public static void main(String[] args) throws Exception {
		String type = "dao";
		String url = "http://localhost:8090/FileSharingDAO";
		if (args.length < 2) {
			System.out
					.println("Usage: java SingleServerStart TYPE[ao|dao] URL");
			System.out
					.println("Example: java SingleServerStart ao http://localhost:8090/FileSharingAO");
			return;
		}
		type = args[0];
		url = args[1];

		if (type.equals("dao")) {
			FileSharingDAO dao = new FileSharingDAOImpl();
			Endpoint.publish(url, dao);
		} else {
			FileSharingAO ao = new FileSharingAOImpl();
			Endpoint.publish(url, ao);
		}
		System.out.println("service published at: " + url);

	}

}
