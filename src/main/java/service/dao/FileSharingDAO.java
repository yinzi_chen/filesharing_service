package service.dao;

public interface FileSharingDAO extends FileDAO, UserDAO, UserFileDAO,
		SharedFileDAO, FileContentDAO {

}
