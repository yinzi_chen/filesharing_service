package service.dao;

import entity.File;

public interface FileDAO {
	File createFile(String fileId, File file);

	File getFile(String fileId);

	File updateFile(String fileId, File file);

	boolean deleteFile(String fileId);

}
