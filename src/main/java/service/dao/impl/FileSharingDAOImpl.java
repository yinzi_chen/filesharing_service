package service.dao.impl;

import java.util.List;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.hibernate.Query;
import org.hibernate.Session;

import service.dao.FileSharingDAO;
import util.HibernateUtil;
import util.SessionUtil;
import entity.File;
import entity.FileContent;
import entity.FileStatus;
import entity.SharedFile;
import entity.User;
import entity.UserFile;

@WebService()
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class FileSharingDAOImpl implements FileSharingDAO {

	@Override
	public File createFile(String fileId, File file) {
		return (File) SessionUtil.create(fileId, file);
	}

	@Override
	public File getFile(String fileId) {
		return (File) SessionUtil.get(fileId, File.class, fileId);
	}

	@Override
	public File updateFile(String fileId, File file) {
		return (File) SessionUtil.update(fileId, file);
	}

	@Override
	public boolean deleteFile(String fileId) {
		File file = new File();
		file.setFileId(fileId);
		return SessionUtil.delete(fileId, file);
	}

	@Override
	public User createUser(String userId, User user) {
		return (User) SessionUtil.create(userId, user);
	}

	@Override
	public User getUser(String userId) {
		return (User) SessionUtil.get(userId, User.class, userId);
	}

	@Override
	public User updateUser(String userId, User user) {
		return (User) SessionUtil.update(userId, user);
	}

	@Override
	public SharedFile createSharedFile(String ownerUserId, SharedFile file) {
		return (SharedFile) SessionUtil.create(ownerUserId, file);
	}

	@Override
	public SharedFile[] findSharedFiles(String ownerUserId) {
		Session session = HibernateUtil.getSessionFactory(ownerUserId)
				.openSession();
		try {
			session.beginTransaction();

			Query query = session
					.createQuery("from SharedFile where ownerUserId = ?")
					.setString(0, ownerUserId).setCacheable(true);
			List list = query.list();
			int size = list.size();
			SharedFile[] res = new SharedFile[size];
			for (int i = 0; i < size; ++i) {
				res[i] = (SharedFile) list.get(i);
			}

			session.getTransaction().commit();
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public SharedFile[] findSharedFilesByStatus(String ownerUserId, int status) {
		Session session = HibernateUtil.getSessionFactory(ownerUserId)
				.openSession();
		try {
			session.beginTransaction();

			Query query = session
					.createSQLQuery(
							"select * from SharedFile where ownerUserId = ? and ((status & ?) != 0)")
					.setString(0, ownerUserId).setInteger(1, status)
					.setCacheable(false);
			List list = query.list();
			int size = list.size();
			SharedFile[] res = new SharedFile[size];
			for (int i = 0; i < size; ++i) {
				res[i] = (SharedFile) list.get(i);
			}

			session.getTransaction().commit();
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public SharedFile updateSharedFile(String ownerUserId, SharedFile file) {
		return (SharedFile) SessionUtil.update(ownerUserId, file);
	}

	@Override
	public UserFile createUserFile(String userId, UserFile userFile) {
		return (UserFile) SessionUtil.create(userId, userFile);
	}

	@Override
	public UserFile[] findUserFiles(String userId) {
		Session session = HibernateUtil.getSessionFactory(userId).openSession();
		try {
			session.beginTransaction();

			Query query = session
					.createQuery(
							"from UserFile where userId = ? and (owner is true or status = ?)")
					.setString(0, userId).setInteger(1, FileStatus.ACCEPTED)
					.setCacheable(true);
			List list = query.list();
			int size = list.size();
			UserFile[] res = new UserFile[size];
			for (int i = 0; i < size; ++i) {
				res[i] = (UserFile) list.get(i);
			}

			session.getTransaction().commit();
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public UserFile[] findUserFilesByDir(String userId, long parentId) {
		Session session = HibernateUtil.getSessionFactory(userId).openSession();
		try {
			session.beginTransaction();

			Query query = session
					.createQuery(
							"from UserFile where userId = ? and parentId = ? and (owner is true or status = ?)")
					.setString(0, userId).setLong(1, parentId)
					.setInteger(2, FileStatus.ACCEPTED).setCacheable(true);
			List list = query.list();
			int size = list.size();
			UserFile[] res = new UserFile[size];
			for (int i = 0; i < size; ++i) {
				res[i] = (UserFile) list.get(i);
			}

			session.getTransaction().commit();
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public UserFile[] findUserFilesByStatusPermission(String userId,
			int status, int permission) {
		Session session = HibernateUtil.getSessionFactory(userId).openSession();
		try {
			session.beginTransaction();

			Query query = session
					.createSQLQuery(
							"select * from UserFile where userId = ? and ((status & ?) != 0) and ((permission & ?) != 0)")
					.setString(0, userId).setInteger(1, status)
					.setInteger(2, permission).setCacheable(false);
			List list = query.list();
			int size = list.size();
			UserFile[] res = new UserFile[size];
			for (int i = 0; i < size; ++i) {
				res[i] = (UserFile) list.get(i);
			}

			session.getTransaction().commit();
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public UserFile updateUserFile(String userId, UserFile userFile) {
		return (UserFile) SessionUtil.update(userId, userFile);
	}

	@Override
	public boolean deleteUserFile(String userId, long id) {
		UserFile file = new UserFile();
		file.setUserId(userId);
		file.setId(id);
		return SessionUtil.delete(userId, file);
	}

	@Override
	public UserFile getUserFile(String userId, long id) {
		return (UserFile) SessionUtil.get(userId, UserFile.class, id);
	}

	@Override
	public FileContent createFileContent(String fileId, FileContent fileContent) {
		return (FileContent) SessionUtil.create(fileId, fileContent);
	}

	@Override
	public FileContent getFileContent(String fileId) {
		return (FileContent) SessionUtil.get(fileId, FileContent.class, fileId);
	}

	@Override
	public FileContent updateFileContent(String fileId, FileContent fileContent) {
		return (FileContent) SessionUtil.update(fileId, fileContent);
	}

	@Override
	public boolean deleteFileContent(String fileId) {
		FileContent fileContent = new FileContent();
		fileContent.setFileId(fileId);
		return SessionUtil.delete(fileId, fileContent);
	}

}
