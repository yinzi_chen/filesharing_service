package service.dao.impl.deprecated;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import service.dao.FileDAO;
import util.SessionUtil;
import entity.File;

@WebService()
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class FileDAOImpl implements FileDAO {

	@Override
	public File createFile(String fileId, File file) {
		return (File) SessionUtil.create(fileId, file);
	}

	@Override
	public File getFile(String fileId) {
		return (File) SessionUtil.get(fileId, File.class, fileId);
	}

	@Override
	public File updateFile(String fileId, File file) {
		return (File) SessionUtil.update(fileId, file);
	}

	@Override
	public boolean deleteFile(String fileId) {
		File file = new File();
		file.setFileId(fileId);
		return SessionUtil.delete(fileId, file);
	}

}
