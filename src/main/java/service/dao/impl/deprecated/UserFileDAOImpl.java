package service.dao.impl.deprecated;

import java.util.List;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.hibernate.Query;
import org.hibernate.Session;

import service.dao.UserFileDAO;
import util.HibernateUtil;
import util.SessionUtil;
import entity.UserFile;

@WebService()
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class UserFileDAOImpl implements UserFileDAO {

	@Override
	public UserFile createUserFile(String userId, UserFile userFile) {
		return (UserFile) SessionUtil.create(userId, userFile);
	}

	@Override
	public UserFile[] findUserFiles(String userId) {
		Session session = HibernateUtil.getSessionFactory(userId).openSession();
		try {
			session.beginTransaction();

			Query query = session.createQuery("from UserFile where userId = ?")
					.setString(0, userId).setCacheable(true);
			List list = query.list();
			int size = list.size();
			UserFile[] res = new UserFile[size];
			for (int i = 0; i < size; ++i) {
				res[i] = (UserFile) list.get(i);
			}

			session.getTransaction().commit();
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public UserFile[] findUserFilesByDir(String userId, long parentId) {
		Session session = HibernateUtil.getSessionFactory(userId).openSession();
		try {
			session.beginTransaction();

			Query query = session
					.createQuery(
							"from UserFile where userId = ? and parentId = ?")
					.setString(0, userId).setLong(1, parentId)
					.setCacheable(true);
			List list = query.list();
			int size = list.size();
			UserFile[] res = new UserFile[size];
			for (int i = 0; i < size; ++i) {
				res[i] = (UserFile) list.get(i);
			}

			session.getTransaction().commit();
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public UserFile[] findUserFilesByStatusPermission(String userId,
			int status, int permission) {
		Session session = HibernateUtil.getSessionFactory(userId).openSession();
		try {
			session.beginTransaction();

			Query query = session
					.createSQLQuery(
							"select * from UserFile where userId = ? and ((status & ?) != 0) and ((permission & ?) != 0)")
					.setString(0, userId).setInteger(1, status)
					.setInteger(2, permission).setCacheable(false);
			List list = query.list();
			int size = list.size();
			UserFile[] res = new UserFile[size];
			for (int i = 0; i < size; ++i) {
				res[i] = (UserFile) list.get(i);
			}

			session.getTransaction().commit();
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public UserFile updateUserFile(String userId, UserFile userFile) {
		return (UserFile) SessionUtil.update(userId, userFile);
	}

	@Override
	public boolean deleteUserFile(String userId, long id) {
		UserFile file = new UserFile();
		file.setUserId(userId);
		file.setId(id);
		return SessionUtil.delete(userId, file);
	}

	@Override
	public UserFile getUserFile(String userId, long id) {
		return (UserFile) SessionUtil.get(userId, UserFile.class, id);
	}

}
