package service.dao.impl.deprecated;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import service.dao.UserDAO;
import util.SessionUtil;
import entity.User;

@WebService()
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class UserDAOImpl implements UserDAO {
	// @WebMethod
	// public String sayHello(@WebParam(name = "userName") String userName) {
	// return "hi," + userName + " welcom to www.micmiu.com";
	// }

	@Override
	public User createUser(String userId, User user) {
		return (User) SessionUtil.create(userId, user);
	}

	@Override
	public User getUser(String userId) {
		return (User) SessionUtil.get(userId, User.class, userId);
	}

	@Override
	public User updateUser(String userId, User user) {
		return (User) SessionUtil.update(userId, user);
	}

}
