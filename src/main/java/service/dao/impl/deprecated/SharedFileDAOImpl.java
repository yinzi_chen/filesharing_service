package service.dao.impl.deprecated;

import java.util.List;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.hibernate.Query;
import org.hibernate.Session;

import service.dao.SharedFileDAO;
import util.HibernateUtil;
import util.SessionUtil;
import entity.SharedFile;

@WebService()
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class SharedFileDAOImpl implements SharedFileDAO {

	@Override
	public SharedFile createSharedFile(String ownerUserId, SharedFile file) {
		return (SharedFile) SessionUtil.create(ownerUserId, file);
	}

	@Override
	public SharedFile[] findSharedFiles(String ownerUserId) {
		Session session = HibernateUtil.getSessionFactory(ownerUserId)
				.openSession();
		try {
			session.beginTransaction();

			Query query = session
					.createQuery("from SharedFile where ownerUserId = ?")
					.setString(0, ownerUserId).setCacheable(true);
			List list = query.list();
			int size = list.size();
			SharedFile[] res = new SharedFile[size];
			for (int i = 0; i < size; ++i) {
				res[i] = (SharedFile) list.get(i);
			}

			session.getTransaction().commit();
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public SharedFile[] findSharedFilesByStatus(String ownerUserId, int status) {
		Session session = HibernateUtil.getSessionFactory(ownerUserId)
				.openSession();
		try {
			session.beginTransaction();

			Query query = session
					.createSQLQuery(
							"select * from SharedFile where ownerUserId = ? and ((status & ?) != 0)")
					.setString(0, ownerUserId).setInteger(1, status)
					.setCacheable(false);
			List list = query.list();
			int size = list.size();
			SharedFile[] res = new SharedFile[size];
			for (int i = 0; i < size; ++i) {
				res[i] = (SharedFile) list.get(i);
			}

			session.getTransaction().commit();
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public SharedFile updateSharedFile(String ownerUserId, SharedFile file) {
		return (SharedFile) SessionUtil.update(ownerUserId, file);
	}

}
