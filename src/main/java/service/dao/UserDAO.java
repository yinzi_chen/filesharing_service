package service.dao;

import entity.User;

public interface UserDAO {

	User createUser(String userId, User user);

	User getUser(String userId);

	User updateUser(String userId, User user);
}
