package service.dao;

import entity.SharedFile;

public interface SharedFileDAO {

	SharedFile createSharedFile(String ownerUserId, SharedFile file);

	SharedFile[] findSharedFiles(String ownerUserId);

	SharedFile[] findSharedFilesByStatus(String ownerUserId, int status);

	SharedFile updateSharedFile(String ownerUserId, SharedFile file);

}
