package service.dao;

import entity.FileContent;

public interface FileContentDAO {

	FileContent createFileContent(String fileId, FileContent fileContent);

	FileContent getFileContent(String fileId);

	FileContent updateFileContent(String fileId, FileContent fileContent);

	boolean deleteFileContent(String fileId);

}
