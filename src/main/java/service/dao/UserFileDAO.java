package service.dao;

import entity.UserFile;

public interface UserFileDAO {

	UserFile createUserFile(String userId, UserFile userFile);

	UserFile getUserFile(String userId, long id);

	UserFile[] findUserFiles(String userId);

	UserFile[] findUserFilesByDir(String userId, long parentId);

	UserFile[] findUserFilesByStatusPermission(String userId, int status,
			int permission);

	UserFile updateUserFile(String userId, UserFile userFile);

	boolean deleteUserFile(String userId, long id);

}
