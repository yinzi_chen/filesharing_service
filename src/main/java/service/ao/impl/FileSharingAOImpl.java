package service.ao.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import service.ServiceConfig;
import service.ao.FileSharingAO;
import client.dao.File;
import client.dao.FileContent;
import client.dao.FileSharingDAOImpl;
import client.dao.FileSharingDAOImplService;
import client.dao.SharedFile;
import client.dao.User;
import client.dao.UserFile;
import entity.FileStatus;
import entity.IDGenerator;

@WebService()
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class FileSharingAOImpl implements FileSharingAO {

	@Override
	public boolean register(String userId, User user) {
		try {
			FileSharingDAOImpl dao = getDao(userId);
			User theUser = dao.createUser(user.getUserId(), user);
			// mkdir(theUser.getUserId(), 0, "Share");
			return theUser != null;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public User login(String userId, String password) {
		try {
			FileSharingDAOImpl dao = getDao(userId);
			User user = dao.getUser(userId);
			if (!user.getPassword().equals(password)) {
				return null;
			}
			return user;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean updateProfile(String userId, User user) {
		try {
			FileSharingDAOImpl dao = getDao(userId);
			User theUser = dao.updateUser(user.getUserId(), user);
			// mkdir(theUser.getUserId(), 0, "Share");
			return theUser != null;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public UserFile[] dirLookUp(String userId, long parentId) {
		try {
			FileSharingDAOImpl dao = getDao(userId);
			List<UserFile> list = dao.findUserFilesByDir(userId, parentId)
					.getItem();
			int size = list.size();
			UserFile[] files = new UserFile[size];
			for (int i = 0; i < size; ++i) {
				files[i] = list.get(i);
			}
			return files;
		} catch (Exception e) {
			return new UserFile[0];
		}
	}

	@Override
	public UserFile[] dirLookUpAll(String userId) {
		try {
			FileSharingDAOImpl dao = getDao(userId);
			List<UserFile> list = dao.findUserFiles(userId).getItem();
			int size = list.size();
			UserFile[] files = new UserFile[size];
			for (int i = 0; i < size; ++i) {
				files[i] = list.get(i);
			}
			return files;
		} catch (Exception e) {
			return new UserFile[0];
		}
	}

	@Override
	public boolean rename(String userId, long id, String newName) {
		try {
			FileSharingDAOImpl dao = getDao(userId);
			UserFile file = dao.getUserFile(userId, id);
			file.setFileName(newName);
			return dao.updateUserFile(userId, file) != null;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean move(String userId, long id, long parentId) {
		try {
			FileSharingDAOImpl dao = getDao(userId);
			UserFile file = dao.getUserFile(userId, id);
			file.setParentId(parentId);
			return dao.updateUserFile(userId, file) != null;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean delete(String userId, long id) {
		try {
			FileSharingDAOImpl dao = getDao(userId);
			return dao.deleteUserFile(userId, id);
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public UserFile mkdir(String userId, long parentId, String dirName) {
		try {
			FileSharingDAOImpl dao = getDao(userId);
			UserFile file = new UserFile();
			file.setUserId(userId);
			file.setFileName(dirName);
			file.setParentId(parentId);
			file.setDirectory(true);
			file.setOwner(true);
			file.setPermission(0xffffffff);
			file.setStatus(0xffffffff);
			return dao.createUserFile(userId, file);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public UserFile createFile(String userId, long parentId, String fileName,
			long fileSize, int permission, byte[] data) {
		try {
			FileSharingDAOImpl dao = getDao(userId);

			File file = new File();

			String fileId = IDGenerator.UUID();

			file.setFileId(fileId);
			file.setOwnerUserId(userId);
			file.setFileSize(fileSize);
			file.setCreateTime(System.currentTimeMillis() / 1000);
			file.setPermission(permission);
			dao.createFile(fileId, file);

			FileContent fileContent = new FileContent();
			fileContent.setFileId(fileId);
			fileContent.setData(data);
			dao.createFileContent(fileId, fileContent);

			UserFile userFile = new UserFile();
			userFile.setFileId(fileId);
			userFile.setUserId(userId);
			userFile.setFileName(fileName);
			userFile.setParentId(parentId);
			userFile.setDirectory(false);
			userFile.setOwner(true);
			userFile.setPermission(0xffffffff);
			userFile.setStatus(0xffffffff);
			return dao.createUserFile(userId, userFile);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public boolean updatePublicPermission(String fileId, int permission) {
		try {
			FileSharingDAOImpl dao = getDao(fileId);
			File file = dao.getFile(fileId);
			file.setPermission(permission);
			return dao.updateFile(fileId, file) != null;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean shareFile(String ownerUserId, String receiverUserId,
			String fileId, String fileName, int permission) {
		try {
			FileSharingDAOImpl dao = getDao(ownerUserId);
			SharedFile sharedFile = new SharedFile();
			sharedFile.setOwnerUserId(ownerUserId);
			sharedFile.setReceiverUserId(receiverUserId);
			sharedFile.setFileId(fileId);
			// sharedFile.setStatus(FileStatus.PENDING);
			dao.createSharedFile(ownerUserId, sharedFile);

			UserFile file = new UserFile();
			file.setFileId(fileId);
			file.setUserId(receiverUserId);
			file.setFileName(fileName);
			file.setParentId(0);
			file.setDirectory(false);
			file.setOwner(false);
			file.setPermission(permission);
			file.setStatus(FileStatus.PENDING);
			return dao.createUserFile(receiverUserId, file) != null;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public UserFile[] getSharePendingFiles(String userId) {
		try {
			FileSharingDAOImpl dao = getDao(userId);
			List<UserFile> list = dao.findUserFilesByStatusPermission(userId,
					FileStatus.PENDING, 0xffffffff).getItem();
			int size = list.size();
			UserFile[] files = new UserFile[size];
			for (int i = 0; i < size; ++i) {
				files[i] = list.get(i);
			}
			return files;
		} catch (Exception e) {
			return new UserFile[0];
		}
	}

	@Override
	public boolean acceptSharedFile(String userId, long id, long parentId) {
		try {
			FileSharingDAOImpl dao = getDao(userId);
			UserFile file = dao.getUserFile(userId, id);
			file.setStatus(FileStatus.ACCEPTED);
			file.setParentId(parentId);
			return dao.updateUserFile(userId, file) != null;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public SharedFile[] getSharedFiles(String userId) {
		try {
			FileSharingDAOImpl dao = getDao(userId);
			List<SharedFile> list = dao.findSharedFiles(userId).getItem();
			int size = list.size();
			SharedFile[] files = new SharedFile[size];
			for (int i = 0; i < size; ++i) {
				files[i] = list.get(i);
			}
			return files;
		} catch (Exception e) {
			return new SharedFile[0];
		}
	}

	@Override
	public File getFile(String fileId) {
		try {
			FileSharingDAOImpl dao = getDao(fileId);
			return dao.getFile(fileId);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public FileContent downloadFile(String fileId) {
		try {
			FileSharingDAOImpl dao = getDao(fileId);
			return dao.getFileContent(fileId);
		} catch (Exception e) {
			return null;
		}
	}

	private FileSharingDAOImpl getDao(String key) {
		// http://localhost:8090/FileSharingDAOService?wsdl
		String[] addr = ServiceConfig.get("FileSharingDAO").split(";");
		int size = addr.length;
		int idx = Math.abs(key.hashCode()) % size;
		FileSharingDAOImplService service;
		System.out.println(addr[idx]);
		try {
			service = new FileSharingDAOImplService(new URL(addr[idx]));
			System.out.println("Sending request to " + addr[idx]);
			return service.getFileSharingDAOImplPort();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
