package service.ao;

import client.dao.File;
import client.dao.FileContent;
import client.dao.SharedFile;
import client.dao.User;
import client.dao.UserFile;

public interface FileSharingAO {
	public boolean register(String userId, User user);

	public User login(String userId, String password);

	public boolean updateProfile(String userId, User user);

	public UserFile[] dirLookUp(String userId, long parentId);

	public UserFile[] dirLookUpAll(String userId);

	public SharedFile[] getSharedFiles(String userId);

	public boolean rename(String userId, long id, String newName);

	public boolean move(String userId, long id, long parentId);

	public boolean delete(String userId, long id);

	public UserFile mkdir(String userId, long parentId, String dirName);

	public UserFile createFile(String userId, long parentId, String fileName,
			long fileSize, int permission, byte[] data);

	public FileContent downloadFile(String fileId);

	// permission for public access
	public boolean updatePublicPermission(String fileId, int permission);

	// share file to specific user
	public boolean shareFile(String ownerUserId, String receiverUserId,
			String fileId, String fileName, int permission);

	public boolean acceptSharedFile(String userId, long id, long parentId);

	public UserFile[] getSharePendingFiles(String userId);

	public File getFile(String fileId);

}
