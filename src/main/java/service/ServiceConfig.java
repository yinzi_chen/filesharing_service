package service;

import java.io.FileInputStream;
import java.util.Properties;

public class ServiceConfig {

	static Properties properties;

	static {
		properties = new Properties();
		try {
			FileInputStream fis = new FileInputStream("services.cfg");
			properties.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String get(String key) {
		return properties.getProperty(key);
	}
}
